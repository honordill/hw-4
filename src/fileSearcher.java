import java.io.File;
import java.io.IOException;

public class fileSearcher {
    static String smth = "Not Found!";

    public static String searcher(String Path, String findIt) throws IOException {
        File currentFolder = new File(Path);
        File[] folderEntries = currentFolder.listFiles();
        for (File entry : folderEntries) {
            if (entry.getName().equals(findIt)) {
                smth = entry.getPath();
                break;
            }
            if (entry.isDirectory()) {
                searcher(entry.getPath(), findIt);
            }
        }
        return smth;
    }
}


